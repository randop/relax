# Relax

<p>
  <img src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  <img alt="Maintenance" src="https://img.shields.io/badge/Maintained%3F-yes-green.svg" target="_blank" />
</p>

## Setup
```bash
npm install
```

## Thanks for support
> Please help this project.

## Author

👤 **Randolph Ledesma**

* 📱 +1 (415) 754-3092
* 🌐 [https://gitlab.com/randop](https://gitlab.com/randop)
* 👷 [https://www.linkedin.com/in/randop/](https://www.linkedin.com/in/randop/)
* 📍 Philippines

## 📝 License

Copyright © 2023 [Randolph Ledesma](https://gitlab.com/randop).